use mmtk::Plan;
use mmtk::vm::ActivePlan;
use mmtk::util::opaque_pointer::*;
use mmtk::Mutator;
use crate::types::StgWord;
use crate::GHCVM;
use crate::SINGLETON;

extern "C" {
    fn upcall_is_capability(tls: VMThread) -> bool;
    fn upcall_get_mutator(tls: VMMutatorThread) -> *mut Mutator<GHCVM>;
    static mut n_capabilities: StgWord;
}

pub struct VMActivePlan<> {}

impl ActivePlan<GHCVM> for VMActivePlan {
    fn global() -> &'static dyn Plan<VM=GHCVM> {
        SINGLETON.get_plan()
    }

    fn number_of_mutators() -> usize {
        unsafe {n_capabilities}
    }

    fn is_mutator(tls: VMThread) -> bool {
        unsafe {upcall_is_capability(tls)}
    }

    fn mutator(tls: VMMutatorThread) -> &'static mut Mutator<GHCVM> {
        unsafe { &mut *upcall_get_mutator(tls) }
    }

    fn reset_mutator_iterator() {
        unimplemented!()
    }

    fn get_next_mutator() -> Option<&'static mut Mutator<GHCVM>> {
        unimplemented!()
    }
}
