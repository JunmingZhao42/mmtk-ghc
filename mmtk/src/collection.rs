use std::sync::atomic::{AtomicBool, Ordering};
use mmtk::vm::Collection;
use mmtk::vm::GCThreadContext;
use mmtk::MutatorContext;
use mmtk::util::opaque_pointer::*;
use mmtk::scheduler::*;
use super::types::StgWord;
use crate::GHCVM;

pub struct VMCollection {}

static mut MMTK_GC_PENDING: AtomicBool = AtomicBool::new(false);

#[repr(C)]
struct Task (usize);

extern "C" {
// import/define global flag mmtk_gc_flag
  fn getMyTask() -> *const Task;
  fn stopAllCapabilitiesForMMTK(cap: VMWorkerThread, task: *const Task);
  fn releaseAllCapabilities(n_capabilities: usize, cap: VMWorkerThread, task: *const Task);
  fn yieldCapability(cap: VMMutatorThread, task: *const Task, did_gc_last: bool);
  fn upcall_spawn_gc_controller(tls: VMThread, controller: *mut GCController<GHCVM>);
  fn upcall_spawn_gc_worker(tls: VMThread, worker: *mut GCWorker<GHCVM>);
  static mut n_capabilities: StgWord;
}

impl Collection<GHCVM> for VMCollection {
    fn stop_all_mutators<E: ProcessEdgesWork<VM=GHCVM>>(tls: VMWorkerThread) {
        unsafe {
            // FIXME: This should probably rather be cap->running_task
            let task = getMyTask();
            stopAllCapabilitiesForMMTK(tls, task);
        }
    }

    fn resume_mutators(tls: VMWorkerThread) {
        unsafe {
            // FIXME: This should probably rather be cap->running_task
            let task = getMyTask();
            MMTK_GC_PENDING.store(false, Ordering::SeqCst);
            releaseAllCapabilities(n_capabilities, tls, task);
        }
    }

    fn block_for_gc(tls: VMMutatorThread) {
        // need to block when gc is blocked
        // FIXME: This should probably rather be cap->running_task
        unsafe {
            MMTK_GC_PENDING.store(true, Ordering::SeqCst);
            let task = getMyTask();
            while !MMTK_GC_PENDING.load(Ordering::SeqCst) {
                yieldCapability(tls, task, false);
            }
        }
        // yieldCapability(tls, task, false); // TODO: Figure out what to do about did_gc_last
    }

    fn spawn_gc_thread(tls: VMThread, ctx: GCThreadContext<GHCVM>) {
        unsafe {
            match ctx {
                GCThreadContext::Controller(controller) => upcall_spawn_gc_controller(tls, Box::into_raw(controller)),
                GCThreadContext::Worker(worker) => upcall_spawn_gc_worker(tls, Box::into_raw(worker)),
            }
        }
    }

    fn prepare_mutator<T: MutatorContext<GHCVM>>(_tls_w: VMWorkerThread, _tls_m: VMMutatorThread, _mutator: &T) {
    }

    // TODO: handle schedule_finalization, process_weak_refs
}
